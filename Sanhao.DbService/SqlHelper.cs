using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Sanhao.DbService
{
    public class SqlHelper : DbHelper
    {
        public SqlHelper(string strConn) : base(DbProvider.SqlServer, strConn)
        {
        }

        public override void BulkInsert(DataTable source, string table)
        {
            using (DbConnection conn = CurrentDbFactory.CreateDbConnection(ConnectionString))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(conn as SqlConnection); //用其它源的数据有效批量加载sql server表中
                bulkCopy.DestinationTableName = table; //服务器上目标表的名称
                bulkCopy.BatchSize = source.Rows.Count; //每一批次中的行数

                try
                {
                    conn.Open();
                    if (source != null && source.Rows.Count != 0)
                        bulkCopy.WriteToServer(source); //将提供的数据源中的所有行复制到目标表中
                }
                catch (Exception)
                {

                }
                finally
                {
                    conn.Close();
                    if (bulkCopy != null)
                        bulkCopy.Close();
                }
            }

        }
    }
}