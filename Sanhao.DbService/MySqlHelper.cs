using System;
using System.Data;
using System.Data.Common;
using MySqlConnector;

namespace Sanhao.DbService
{
    public class MySqlHelper : DbHelper
    {
        public MySqlHelper(string strConn) : base(DbProvider.MySql, strConn)
        {
        }

        public override void BulkInsert(DataTable source, string table)
        {
            using (DbConnection conn = CurrentDbFactory.CreateDbConnection(ConnectionString))
            {
                MySqlBulkCopy bulkCopy = new MySqlBulkCopy(conn as MySqlConnection); 
                bulkCopy.DestinationTableName = table; 
                try
                {
                    conn.Open();
                    if (source != null && source.Rows.Count != 0)
                        bulkCopy.WriteToServer(source); //将提供的数据源中的所有行复制到目标表中
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }

        }

    }
}


